/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9part2;

/**
 *
 * @author Admin
 */
public abstract class Vegetable {

private String colour;

private int size;

private String name;

Vegetable(String colour, int size, String name){

this.colour = colour;

this.size = size;

this.name = name;

}

abstract boolean isRipe();

public String getColour(){

return this.colour;

}

public int getSize(){

return this.size;

}

public String getName(){

return this.name;

}
    
}
